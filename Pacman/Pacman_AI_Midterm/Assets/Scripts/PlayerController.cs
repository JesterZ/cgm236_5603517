﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float spd;
    public Transform dirObject;
    public bool onFire;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {
            transform.rotation = Quaternion.Euler(0,180,0);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.rotation = Quaternion.Euler(0,0,0);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            transform.rotation = Quaternion.Euler(0,0,-90);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            transform.rotation = Quaternion.Euler(0, 0,90);
        }

    }
    private void FixedUpdate()
    {
        int layer_mask = LayerMask.GetMask("Wall");
        Vector2 pos = gameObject.transform.position;
        Debug.DrawLine(new Vector2(dirObject.transform.position.x, dirObject.transform.position.y), pos, Color.red);
        RaycastHit2D hit = Physics2D.Linecast(new Vector2(dirObject.transform.position.x, dirObject.transform.position.y),pos,layer_mask);
        if (hit.collider == null)
        {
            transform.Translate(Vector3.right * Time.deltaTime * spd);
        }
        if(onFire == true)
        {
            StartCoroutine("Return");
        }
    }
    IEnumerator Return()
    {
        yield return new WaitForSeconds(3.0f);
        onFire = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warpgate : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Pacman_Player" || collision.name == "Ghost_Red" || collision.name == "Ghost_Blue" || collision.name == "Ghost_Green")
        {
            if (gameObject.transform.position.x > 0)
            {
                collision.gameObject.transform.position = new Vector3(-6.0f, 0.254f,0);
            }
            if (gameObject.transform.position.x < 0)
            {
                collision.gameObject.transform.position = new Vector3(6.0f, 0.254f, 0);
            }
        }
    }
}

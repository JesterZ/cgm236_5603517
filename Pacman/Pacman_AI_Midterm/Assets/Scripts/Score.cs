﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public static int scoreP;
    public Text scoreText;
    public GameObject UI;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "Score: "+scoreP;
        if(scoreP >= 8900)
        {
            Time.timeScale = 0.0f;
            UI.SetActive(true);
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Secial : MonoBehaviour {
    public GameObject playerObj;
    public PlayerController player_Script;
    // Use this for initialization
    void Start () {
        playerObj = GameObject.FindWithTag("Player");
        if (playerObj != null)
        {
            player_Script = playerObj.GetComponent<PlayerController>();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
            player_Script.onFire = true;
        }
    }
}

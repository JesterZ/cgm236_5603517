﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMovement : MonoBehaviour {

    public GameObject baricade;
    public GameObject playerObj;
    public PlayerController player_Script;
    public Transform playerPos;
    public Transform dirObject;
    public Rigidbody2D rb;
    private float spd;
    public bool startUp;
    // Use this for initialization
    void Start() {
        spd = 1.5f;
        playerObj = GameObject.FindWithTag("Player");
        if (playerObj != null)
        {
            player_Script = playerObj.GetComponent<PlayerController>();
        }
        startUp = true;
        rb.GetComponent<Rigidbody2D>();
        if (this.gameObject.name == "Ghost_Red")
        {
            rb.velocity = new Vector3(spd, 0, 0);
        }
        if (this.gameObject.name == "Ghost_Blue")
        {
            rb.velocity = new Vector3(0, spd, 0);
        }
        if (this.gameObject.name == "Ghost_Green")
        {
            rb.velocity = new Vector3(-spd, 0, 0);
        }
        StartCoroutine("StartTurn");
    }

    // Update is called once per frame
    void Update() {
        if (player_Script.onFire == false)
        {
            spd = 1.5f;
        }
        if (player_Script.onFire == true)
        {
            spd = 0.5f;
        }
        CheckObstacle();
        CheckPlayer();

    }
    IEnumerator StartTurn()
    {
        yield return new WaitForSeconds(0.46f);
        if (this.gameObject.name == "Ghost_Red" && startUp == true)
        {
            rb.velocity = new Vector3(0, spd, 0); ;
        }
        if (this.gameObject.name == "Ghost_Green" && startUp == true)
        {
            rb.velocity = new Vector3(0,spd, 0);
        }
        yield return new WaitForSeconds(1.0f);
        baricade.SetActive(true);
    }
    public void CheckObstacle()
    {
        int layer_mask = LayerMask.GetMask("Wall");
        Vector2 pos = gameObject.transform.position;
        Debug.DrawLine(new Vector2(dirObject.transform.position.x, dirObject.transform.position.y), pos, Color.red);
        RaycastHit2D hit = Physics2D.Linecast(new Vector2(dirObject.transform.position.x, dirObject.transform.position.y), pos, layer_mask);
        if (hit.collider != null && startUp == false)
        {
            int x = Random.Range(1,100);
            if(x <= 50)
            {
                transform.Rotate(0, 0, 90);
            }
            else if (x >= 51)
            {
                transform.Rotate(0, 0,-90);
            }
        }
    }
    public void CheckPlayer()
    {
        Debug.DrawLine(transform.position, new Vector2(playerPos.transform.position.x, playerPos.transform.position.y), Color.red);
        RaycastHit2D hit = Physics2D.Linecast(transform.position, playerPos.transform.position,LayerMask.GetMask("Player","Wall"));
        if (hit.collider.tag == "Wall" && startUp == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, dirObject.position, spd * Time.deltaTime);
            Debug.Log("Wall");
        }
        else if (hit.collider.tag == "Player" && startUp == false)
        {
            if (player_Script.onFire == false)
            {
                transform.position = Vector3.MoveTowards(transform.position, playerPos.position, spd * Time.deltaTime);
                Debug.Log("Player");
            }
            else if (player_Script.onFire == true)
            {
                transform.position = Vector3.MoveTowards(transform.position, playerPos.position,-1*spd * Time.deltaTime);
                Debug.Log("Player");
            }
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "StartPoint2" && startUp == true) 
        {
            rb.velocity = new Vector3(0, 0, 0);
            startUp = false;
        }
        if (collision.gameObject.name == "Pacman_Player" && player_Script.onFire == false)
        {
            Time.timeScale = 0.0f;
        }
        if (collision.gameObject.name == "Pacman_Player" && player_Script.onFire == true)
        {
            Destroy(this.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCastTest : MonoBehaviour {

    public Transform playerPos;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Debug.DrawLine(transform.position, new Vector2(playerPos.transform.position.x, playerPos.transform.position.y), Color.red);
        RaycastHit2D hit = Physics2D.Linecast(transform.position, playerPos.transform.position);
        if (hit == true)
        {
            Debug.Log(hit.collider);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack_Hw_Week2
{
    class Program
    {
        static void Main(string[] args)
        {
            CStack cStack = new CStack();
            string choice, value;
            string word = "Homework";
            for (int i = 0; i < word.Length; i++)
            {
                cStack.Push(word.Substring(i, 1));
            }
            while (true)
            {
                Console.WriteLine("---------------");
                Console.WriteLine("(1)Top");
                Console.WriteLine("(2)Pop");
                Console.WriteLine("(3)Push");
                Console.WriteLine("(4)Count");
                Console.WriteLine("(5)Clear");
                Console.WriteLine("(6)ShowAll");
                Console.WriteLine("---------------");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] onechar = choice.ToCharArray();

                switch (onechar[0])
                {
                    case '1':
                        if (cStack.Count != 0)
                        {
                            Console.WriteLine(cStack.Top());
                        }
                        else
                        {
                            Console.WriteLine("Array of index's out of range");
                        }
                        break;
                    case '2':
                        if (cStack.Count != 0)
                        {
                            Console.WriteLine(cStack.Top() + " had been remove");
                            Console.WriteLine(cStack.Pop());
                        }
                        else
                        {
                            Console.WriteLine("Array of index's out of range");
                        }
                        break;
                    case '3':
                        Console.WriteLine("Please enter value");
                        value = Console.ReadLine();
                        for (int i = 0; i < value.Length; i++)
                        {
                            cStack.Push(value.Substring(i, 1));
                        }
                        Console.WriteLine("Pushed");
                        break;
                    case '4':
                        Console.WriteLine(cStack.Count);
                        break;
                    case '5':
                        cStack.Clear();
                        Console.WriteLine("Cleared");
                        break;
                    case '6':
                        cStack.ShowAll();
                        break;

                }
            }
        }
    }
}

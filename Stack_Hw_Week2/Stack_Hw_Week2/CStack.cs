﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Stack_Hw_Week2
{
    class CStack
    {
        public int p_index;
        public ArrayList list;

        public CStack()
        {
            list = new ArrayList();
            p_index = -1;
        }
        public int Count
        {
            get
            {
            return list.Count;
            }
        }
        public void Push(Object item)
        {
            list.Add(item);
            p_index++;
        }
        public Object Pop()
        {
                list.RemoveAt(p_index);
                p_index--;
                return null;
        }
        public void Clear()
        {
            list.Clear();
            p_index = -1;
        }
        public Object Top()
        {
            return list[p_index];
        }
        public void ShowAll()
        {
            Console.WriteLine(" ");
            foreach (string lists in list)
            {
                Console.WriteLine(lists + Environment.NewLine);
            }
        }
    }
}

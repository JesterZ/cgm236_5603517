﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BST
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree nums = new BinarySearchTree();
            nums.Insert(23);
            nums.Insert(10);
            nums.Insert(9);
            nums.Insert(55);
            nums.Insert(66);
            nums.Insert(12);

            /* Console.WriteLine("BST  :   Inorder");
             nums.InOrder(nums.root);
             Console.Read();*/
            /*Console.WriteLine("BST  :   Preorder");
            nums.PreOrder(nums.root);
            Console.Read();*/
            /*Console.WriteLine("BST  :   Postorder");
            nums.PostOrder(nums.root);
            Console.Read();*/
            /*Console.WriteLine("Min is:");
            Console.WriteLine(nums.FindMin());*/
            Console.WriteLine("Max is:");
            Console.WriteLine(nums.FindMax());

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList_HW_Week2
{
    class ListIter
    {
        private Node current;
        private Node previous;
        LinkedList theList;

        public ListIter(LinkedList list)
        {
            theList = list;
            current = theList.GetFirst();
            previous = null;
        }
        public void NextLink()
        {
            previous = current;
            current = current.Link;
        }
        public Node GetCurrent()
        {
            return current;
        }

        public void InsertAfter(Object obj)
        {
            Node newNode = new Node(obj);
            newNode.Link = current.Link;
            current.Link = newNode;
            NextLink();
        }

        public void Remove()
        {
            previous.Link = current.Link;
        }

        public void Reset()
        {
            current = theList.GetFirst();
            previous = null;
        }

        public bool AtEnd()
        {
            return (current.Link == null);
        }
    }
}
